#!/usr/bin/env bash

# Make a SONAR docker image with a user matching the current user

tmpdir=$(mktemp -d)
cat << EOF > "$tmpdir/Dockerfile"
FROM scharch/sonar
RUN useradd -l -M -s /bin/bash -u $UID $USER
EOF
docker build -t $USER/sonar "$tmpdir"
rm "$tmpdir/Dockerfile"
rmdir "$tmpdir"
