#!/usr/bin/env bash

# Wrapper script to run commands via SONAR's Docker container.  If I've done
# this right it should be able to act as a drop-in replacement for a
# locally-installed sonar executable (or fastq-dump within the container too)
# when symlinked with a name matching the command to run.  If run directly it
# should start an interactive shell.
#
# Without --network=host, docker will default to its "bridge" (seemingly NAT?)
# network setup, and DISPLAY will reference the wrong localhost.  If X11 access
# control is enabled, the Xauthority data has to be provided, too; bring along
# all of $HOME will include that.
# All of that is to allow the interactive identity/divergence plotting and
# island selection in module 2.
#
# The /project options make the current working directory on the host side also
# the working directory in the container, under /project.

cmd=$(basename "$0")
if [[ $cmd == "docker-wrapper.sh" ]]; then
	docker run -it --network=host --user $USER -e DISPLAY -v $HOME:$HOME -v $PWD:/project -w /project $USER/sonar
else
	docker run --network=host --user $USER -e DISPLAY -v $HOME:$HOME -v $PWD:/project -w /project $USER/sonar "$cmd" "$@"
fi
