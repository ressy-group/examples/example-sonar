#!/usr/bin/env bash

# Like docker-wrapper.sh but way simpler!  The only catch I've found is that
# the pass-through of environment variables can confuse the container with a
# LANG like "en_US.UTF-8", which causes perl to write a few warnings to stderr,
# which causes SONAR to crash.  We can just empty out the LANG variable to
# avoid that.
# https://sylabs.io/guides/3.0/user-guide/environment_and_metadata.html#environment
# https://stackoverflow.com/a/2510548/4499968

cmd=$(basename "$0")
if [[ $cmd == "singularity-wrapper.sh" ]]; then
	SINGULARITYENV_LANG="" singularity run docker://scharch/sonar
else
	SINGULARITYENV_LANG="" singularity exec docker://scharch/sonar "$cmd" "$@"
fi
