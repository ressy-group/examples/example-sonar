# SONAR Example

Following along with the [paper] and [vignette] for [SONAR].

## Singularity method

With [Singularity](https://singularity.hpcng.org/) things almost work out of
the box for a non-root user: the username and ID matches inside the container,
environment variables from the calling environment are passed through, and the
filesystems look the same.  So, things like the interactive plots just work.
There's one catch involving the locale setting described in the [wrapper
script](scripts/singularity-wrapper.sh).

To use the wrapper script to pass through `sonar` and `fastq-dump` commands to
singularity:

    PATH=$PWD/scripts/singularity:$PATH make

## Docker method, with our Ubuntu 16 LTS setup

I needed some extra setup to get this fully working, at least on our older
Ubuntu server.

Make a `$USER/sonar` image:

    ./scripts/docker-setup.sh

This makes an image like the default SONAR one, but with a user whose ID and
name matches the user on the host OS.  This way file ownership will match up
afterward.  (Possibly [user namespace remapping](https://docs.docker.com/engine/security/userns-remap/)
would make this "just work" but I haven't tried to decipher that yet.)

Then, use the wrapper scripts for Docker and run make:

    PATH=$PWD/scripts/docker:$PATH make

That will use `sonar` and `fastq-dump` commands that run within a docker
container from the above image, via a [wrapper script](scripts/docker-wrapper.sh),
with the environment configured to hopefully make the interactive
identity/divergence plots work via X11.  (Some of this may be specific to our
Docker installation; see the script for more details.)

## Conda environment method

**Currently broken.**

First, a very rough setup script to automate a few setup steps.  This creates a
example-sonar conda environment.

    ./scripts/conda-setup.sh

And then:

    conda activate example-sonar
    make

This runs my own version of what `SONAR/sample_data/runVignette.sh` does, based
on a Makefile and with slightly different directory organization.

Or, using the existing runVignette.sh script:

    make vignette_results

## Outline

Some brief notes on how the modules and commands are organized and what the
scripts do at each step.

### Annotation

 1. Blast V (`blast_V`) to create `work/annotate/vgene/<project>_<group>.txt`
    files with BLAST results for V.  Sets up `work/internal/gene_locus.txt` to
    manage species and V/D/J/C gene library settings for this and step 1.2.
 2. Blast J (`blast_J`) and also D and C based on the 3' portion of the
    sequence matched in the V step; creates `work/annotate/jgene/<project>_<group>.txt`,
    `D_<group>txt`, and `C_<group>.txt` with BLAST results for J, D, and C,
    respectively.  Also writes some stats to `output/tables/<project>_vgerm_tophit.txt`
    and `_stat.txt`.
 3. Finalize Assignments (`finalize`) by dereplicating the BLAST results for D,
    J, and C into `work/annotate/jgene/<d|j|c>_tophit_<group>.txt` and then on
    to `output/tables/<project>_<d|j|c>germ_tophit.txt` and
    `output/tables/analysis-WK34_jgerm_stat.txt`, writing the AIRR-formatted table
    `output/tables/<project>_rearrangements.tsv`, and writing a variety of FASTA
    files in `output/sequences/<nucleotide|amino_acid>/`.
 4. Dereplication and Clustering (`cluster_sequences`) to create additional dereplicated FASTA
    (`output/sequences/<nucleotide|amino_acid>/<project>_<goodCDR3|goodVJ>_unique.fa`)
    and add cluster information to the AIRR file from step 1.3.  Writes vsearch
    clustering results to `output/sequences/nucleotide/<project>_goodVJ.cluster`.
 5. Optional single-cell support (`single_cell`) added since publication;
    creates `<project>_rearrangements_single-cell.tsv`.

### Lineage Determination

 1. Calculate identity divergence (`id-div`): identity to known antibodies of
    interest and divergence from assigned germline V genes.  Creates
    `output/tables/<project>_goodVJ_unique_coverage.tab` with table of percent
    coverage of each query sequence to each reference (germline V or antibody of
    interest), and `output/tables/<project>_goodVJ_unique_id-div.tab` with
    identity/divergence percentages.  Alignments used in the calculations are in
    `work/lineage/align/`.
 2. Manual selection of island for lineage of interest (`get_island`), using an
    interactive R plot to circle noteworthy sequences from a scatterplot of the
    data from step 2.1 and save the sequence IDs to `output/tables/islandSeqs.txt`.
 3. Automated iterative search for sequences related to antibodies of interest
    (`intradonor`).  This can be an alternate approach to the manual one in
    step 2.2.  Note the disclaimer in the script about its use with VRC01 versus
    non-VRC01 antibodies.
 4. Group sequences in pseudo-lineages by V, J, and CDR3 (`groups`); this one
    is "unseeded" versus the two "seeded" (with known antibodies of interest)
    methods in 1.2 and 1.3.
 5. Extract FASTA (`getfastafromlist`; `getfasta` is now too vague now that
    there are more FASTA converter scripts) of lineage member sequences in
    preparation for combining timepoints in module 3.  (This is in the utilities
    directory rather than part of the module 2 scripts, but the default workflow
    writes the FASTA files alongside the other per-timepoint nucleotide sequences.
    As described in the vignette this filters each `goodVJ_unique.fasta` file by
    the sequence IDs in each `islandSeqs.txt` file to create
    `output/sequences/nucleotide/<project>_islandSeqs.fa`.)

### Phylogenetic Analysis

 1. Merge timepoints (`merge_time`) from separate analyses using modules 1 and
    2 above by combining the selected lineage member FASTA files in a separate
    analysis directory.  For members that appear in multiple timepoints (with an
    optional identity threshold) this also collapses the repeats down to the
    earliest ("birthday") occurrence.
 2. Build ML Tree (`igphyml`) using [IgPhyML]. (Updated since publication from
    [DNAML].)

[paper]: https://doi.org/10.3389/fimmu.2016.00372
[vignette]: https://github.com/scharch/SONAR/blob/master/vignette.pdf
[SONAR]: https://github.com/scharch/SONAR
[IgPhyML]: https://igphyml.readthedocs.io
[DNAML]: https://doi.org/10.1093/oxfordjournals.molbev.a025575
